import logging
from functools import lru_cache

import requests
from fastapi import HTTPException
from starlette import status

from mensa_api.adapters.locations import LocationIn
from mensa_api.client import API_URL

logger = logging.getLogger(__name__)


@lru_cache(maxsize=15)
def fetch_location(id: int) -> LocationIn:
    url = f"{API_URL}/locations/{id}"

    logger.debug(f"Requesting {url=}")
    response = requests.get(url)
    logger.debug(f"Got {response=}")

    if response.status_code == 404:
        raise HTTPException(status.HTTP_404_NOT_FOUND)

    response_json = response.json()
    logger.debug(f"Decoded {response_json=}")
    if "error" in response_json:
        raise HTTPException(response_json["error"]["code"],
                            response_json["error"]["message"])

    return LocationIn(**response_json)


def fetch_locations() -> list[LocationIn]:
    url = f"{API_URL}/location"

    logger.debug(f"Requesting {url=}")
    response = requests.get(url)
    logger.debug(f"Got {response=}")

    if response.status_code == 404:
        raise HTTPException(status.HTTP_404_NOT_FOUND)

    response_json = response.json()
    logger.debug(f"Decoded {response_json=}")

    result = []

    for location_json in response_json:
        result.append(LocationIn(**location_json))

    return result
