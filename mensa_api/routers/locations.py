import logging

from fastapi import APIRouter

from mensa_api.adapters.locations import adapt_location, adapt_locations
from mensa_api.client.locations import fetch_location, fetch_locations
from mensa_api.models.locations import Location

logger = logging.getLogger(__name__)

router = APIRouter(
    prefix="/locations",
    tags=["locations"],
    responses={404: {"description": "Not Found"}},
)


@router.get("/", response_model=list[Location])
async def get_location_list() -> list[Location]:
    logger.debug(f"Trying to fetch locations")
    locations_in = fetch_locations()
    return adapt_locations(locations_in)


@router.get("/{id}", response_model=Location)
async def get_location_by_id(id: int) -> Location:
    logger.debug(f"Trying to fetch location with {id=}")
    location_in = fetch_location(id)
    location = adapt_location(location_in)
    logger.debug(f"Found location {location}")
    return location
