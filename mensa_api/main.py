import logging

from fastapi import FastAPI

from mensa_api.routers import locations

description = """A subjectively better version of the [STW-ON Mensa API].
It redefines the models and provides better endpoints for easier uses
in the end projects.

[STW-ON Mensa API]: https://api.stw-on.de/
"""

logger = logging.getLogger("mensa_api")
logger.setLevel(logging.DEBUG)

fh = logging.FileHandler('mensa_api.log')
fh.setLevel(logging.DEBUG)

ch = logging.StreamHandler()
ch.setLevel(logging.WARNING)

formatter = logging.Formatter(
    '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)
ch.setFormatter(formatter)
# add the handlers to the logger
logger.addHandler(fh)
logger.addHandler(ch)

app = FastAPI(
    title="STW-ON Better Mensa API",
    description=description,
    version="0.1.0",
    contact={
        "name": "Nikita Karamov",
        "url": "https://www.kytta.dev/",
        "email": "me@kytta.dev",
    },
    license_info={
        "name": "Apache License 2.0",
        "url": "https://spdx.org/licenses/Apache-2.0.html",
    },
    openapi_tags=[
        {
            "name": "locations",
            "description": "Mensas, Cafeterias, Coffeebars.",
        }
    ]
)

app.include_router(locations.router)
