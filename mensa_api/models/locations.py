import datetime
from enum import Enum, IntEnum
from typing import Optional

from pydantic import BaseModel


class Address(BaseModel):
    line1: str
    line2: Optional[str]
    street: Optional[str]
    zip: str
    city: str

    def __str__(self):
        return ", ".join(filter(
            lambda x: x,
            [
                self.line1,
                self.line2,
                self.street,
                self.zip,
                self.city
            ]))


class TimeOfDay(Enum):
    MORNING = "morning"
    NOON = "noon"
    EVENING = "evening"


class DayOfWeek(IntEnum):
    MONDAY = 1
    TUESDAY = 2
    WEDNESDAY = 3
    THURSDAY = 4
    FRIDAY = 5
    SATURDAY = 6
    SUNDAY = 7


class OpeningHours(BaseModel):
    time: TimeOfDay
    start_day: DayOfWeek
    end_day: DayOfWeek
    start_time: datetime.time
    end_time: datetime.time

    def __str__(self):
        return f"{self.start_day.name.title()}–{self.end_day.name.title()} " \
               f"{self.start_time}–{self.end_time} " \
               f"({self.time.name.title()})"


class Location(BaseModel):
    _id: int
    name: str
    address: Address
    opening_hours: list[OpeningHours]

    def __str__(self):
        return f"\"{self.name}\" at {self.address}, " \
               f"open: {'; '.join([str(h) for h in self.opening_hours])}"
