import logging
from enum import IntEnum

from mensa_api.models.locations import Location, OpeningHours

logger = logging.getLogger(__name__)


class DayOfWeekIn(IntEnum):
    NONE = 0
    MONDAY = 1
    TUESDAY = 2
    WEDNESDAY = 3
    THURSDAY = 4
    FRIDAY = 5
    SATURDAY = 6
    SUNDAY = 7


class OpeningHoursIn(OpeningHours):
    start_day: DayOfWeekIn
    end_day: DayOfWeekIn


class LocationIn(Location):
    _id: None
    id: int
    opening_hours: list[OpeningHoursIn]


def adapt_locations(locations_in: list[LocationIn]) -> list[Location]:
    return list(map(adapt_location, locations_in))


def adapt_location(location_in: LocationIn) -> Location:
    logger.debug(f"Got {location_in=}")

    location_in.opening_hours = list(filter(
        lambda oh: oh.start_day != 0 and oh.end_day != 0,
        location_in.opening_hours
    ))

    return Location(
        **location_in.dict(),
        _id=location_in.id
    )
