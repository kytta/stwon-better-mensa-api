import uvicorn

if __name__ == '__main__':
    uvicorn.run("mensa_api.main:app", log_level="info")
