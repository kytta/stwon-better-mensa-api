# stwon-better-mensa-api

A subjectively better version of the [STW-ON Mensa API]. It redefines the models
and provides better endpoints for easier uses in the end projects.

## Licence

Copyright © 2022 [Nikita Karamov]\
Licenced under the [Apache License 2.0]

Uses API endpoints and parts of the documentation from the [STW-ON Mensa API],
also licenced under the [Apache License 2.0].

---

This project is hosted on Codeberg:
<https://codeberg.org/kytta/stwon-better-mensa-api.git>

[Apache License 2.0]: https://spdx.org/licenses/Apache-2.0.html
[Nikita Karamov]: https://www.kytta.dev/
[STW-ON Mensa API]: https://api.stw-on.de/
